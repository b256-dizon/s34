// Create a fetch request using the GET method that will retrieve ALL the to do list items from JSON Placeholder API.
async function fetchData(){

	let result = await fetch("https://jsonplaceholder.typicode.com/todos")

	console.log(result)
	console.log(typeof result)

	let json = await result.json()
	console.log(json)
}

fetchData();

// Using the data retrieved, create an array using the map method to return ONLY the title of every item and print the result in the console.

let titles = [];

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => {
      let fields = json;
      fields.map(function(id) {
      titles.push(id.title);
	});
})

console.log(titles);

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/23")
.then(response => response.json())
.then(json => console.log(json))

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch("https://jsonplaceholder.typicode.com/todos/10")
.then(response => response.json())
.then(json => {
	let id = json.id;
	let title = json.title;
	let status = json.completed;
	console.log(`The item ${id}: \"${title }\" has a status of \"${status}\"`)
})

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 201,
		title: "Created To Do List Item",
		userId: "1"
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		description: "To Update a to do list item by changing the data structure to contain the following properties:",
		title: "Updated To Do List Item",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "03/29/2023",
		status: "Complete"
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/99", {
	method: "DELETE",
})
